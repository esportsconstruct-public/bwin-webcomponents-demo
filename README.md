# BwinWebcomponentsDemo

This project is an example of how to create footer and header using web components (angular elements).\
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.7.

## Build

Run `npm run build:elements` to build the project. The build artifacts will be stored in the `dist/` directory. The script ready to work will be stored in `elements/`

## How to use

We can use elements by connecting the script

```html
<custom-bwin-header-demo></custom-bwin-header-demo>
<custom-bwin-footer-demo></custom-bwin-footer-demo>
<script src="elements/bwin-webcomponents-demo.js"></script>
```
