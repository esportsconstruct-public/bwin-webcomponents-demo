import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import  { Injector} from '@angular/core';
import  { createCustomElement } from '@angular/elements';
import { BwinHeaderDemoComponent } from './bwin-header-demo/bwin-header-demo.component';
import { BwinFooterDemoComponent } from './bwin-footer-demo/bwin-footer-demo.component';

@NgModule({
  declarations: [
    BwinHeaderDemoComponent,
    BwinFooterDemoComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [
    BwinHeaderDemoComponent,
    BwinFooterDemoComponent
  ]
})
export class AppModule {
  constructor(private injector: Injector){
    const elHeader = createCustomElement(BwinHeaderDemoComponent, {injector : this.injector});
    customElements.define('custom-bwin-header-demo', elHeader);

    const elFooter = createCustomElement(BwinFooterDemoComponent, {injector : this.injector});
    customElements.define('custom-bwin-footer-demo', elFooter);
  }

  public ngDoBootstrap(){

  }
}
