import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BwinHeaderDemoComponent } from './bwin-header-demo.component';

describe('BwinHeaderDemoComponent', () => {
  let component: BwinHeaderDemoComponent;
  let fixture: ComponentFixture<BwinHeaderDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BwinHeaderDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BwinHeaderDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
