import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BwinFooterDemoComponent } from './bwin-footer-demo.component';

describe('BwinFooterDemoComponent', () => {
  let component: BwinFooterDemoComponent;
  let fixture: ComponentFixture<BwinFooterDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BwinFooterDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BwinFooterDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
