const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
  const files = [
    './dist/bwin-webcomponents-demo/runtime.js',
    './dist/bwin-webcomponents-demo/polyfills.js',
    './dist/bwin-webcomponents-demo/main.js',
  ]
  await fs.ensureDir('elements')
  await concat(files, 'elements/bwin-webcomponents-demo.js');
  await fs.copyFile('./dist/bwin-webcomponents-demo/styles.css', 'elements/styles.css')
  // await fs.copy('./dist/bwin-webcomponents-demo/assets/', 'elements/assets/' )
})()
